-----------------------------------
--
-- Test to get warm with VHDL again...
-- Jens Kroeger, Dec 2016
-- 
-- kroeger@physi.uni-heidelberg.de
--
----------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use work.components.all;
use work.lcd_components.all;


entity VHDL_WarmUp is
	port (
		-- LED
		LED 			: out std_logic_vector(1 downto 0);
		LED_g			: out std_logic;
		LED_r			: out std_logic;
		
		usr_dipsw	: in std_logic;
		
		-- PushButton
		PushButton 	: in std_logic_vector(2 downto 0);
		
		-- clock
		clk_1			: in std_logic;
		
		-- LCD
		lcd_csn: 				out 	std_logic;             				--//2.5V    //LCD Chip Select
		lcd_d_cn: 				out 	std_logic;            				--//2.5V    //LCD Data / Command Select
		lcd_data: 				inout 	std_logic_vector(7 downto 0); --//2.5V    //LCD Data
		lcd_wen: 				out 	std_logic             				--//2.5V    //LCD Write Enable
		
	);
	end VHDL_WarmUp;
	
architecture WarmUp_arc of VHDL_WarmUp is

	signal pb_db : std_logic_vector(2 downto 0);			-- pushbuttons debounced
	signal dipsw_db : std_logic;								-- dip switch debouced
	
   -- LCD stuff
	signal tolcd:		std_logic_vector(127 downto 0);
	signal tolcd_ena:	std_logic;
	signal lcd_busy:	std_logic;
	signal lcd_ready:	std_logic;
	
	signal counter: 	integer;
	signal counter2: 	integer;
	
begin
	-- debouncer
	db0: debouncer
		port map(
			clk	=> clk_1,
			din	=> PushButton(0),
			dout	=> pb_db(0)
		);
	db1: debouncer
		port map(
			clk	=> clk_1,
			din	=> PushButton(1),
			dout	=> pb_db(1)
		);
	db2: debouncer
		port map(
			clk	=> clk_1,
			din	=> PushButton(2),
			dout	=> pb_db(2)
		);
		
	db3: debouncer
		port map(
			clk	=> clk_1,
			din	=> usr_dipsw,
			dout	=> dipsw_db
		);
	
	-- control LEDs:
--	LED(0) <= not pb_db(0);
--	LED(1) <= not pb_db(1);
	LED(0) <= pb_db(0);
	LED(1) <= pb_db(1);
	
	LED_g <= dipsw_db;
	LED_r <= not dipsw_db;
	

--- LCD
process (pb_db(2), clk_1)	
begin
	if(pb_db(2) = '0')then
		counter 		<= 0;
		counter2		<= 0;
		tolcd		 	<= (others => '0');
				
	elsif(rising_edge(clk_1)) then
		counter <= counter + 1;
		
		if (counter mod 62500000) = 0 then					-- every 0.5 sec, because f=125 MHz -> T=8 ns
		counter2 <= counter2 + 1;
		tolcd <= std_logic_vector(to_unsigned(counter2, tolcd'length));
--		tolcd(3 downto 0) <= "1111";
		end if;
	end if;
end process;

tolcd_ena   <= '1';
	
lcd_c:lcd_controller
	port map(
		din			=> tolcd,	
		clock			=> clk_1,	
		reset_n		=> pb_db(2),
		enable_in	=> tolcd_ena,
		lcd_enable	=> lcd_csn,
		lcd_rs		=> lcd_d_cn,								
		lcd_rw		=> lcd_wen,					
		busy			=>	lcd_busy,				
		lcd_data		=> lcd_data,
		ready_out	=> lcd_ready
		
		);			
	
	
end WarmUp_arc;