-----------------------------------
--
-- Test to get warm with VHDL again...
-- Jens Kroeger, Dec 2016
-- 
-- kroeger@physi.uni-heidelberg.de
--
----------------------------------

library ieee;
use ieee.std_logic_1164.all;

package components is

component debouncer is
	port (
		clk:	in std_logic;
		din:	in std_logic;
		dout: out std_logic
	);
end component;


end package components;